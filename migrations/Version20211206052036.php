<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211206052036 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE clients (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, client_id VARCHAR(255) NOT NULL, bonus_amount INTEGER DEFAULT NULL)');
        $this->addSql('CREATE TABLE current_deals (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, deal_id VARCHAR(255) NOT NULL, client_id VARCHAR(255) NOT NULL, bonuses INTEGER NOT NULL)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE clients');
        $this->addSql('DROP TABLE current_deals');
    }
}
