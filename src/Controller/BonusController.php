<?php

namespace App\Controller;

use App\Entity\Clients;
use App\Entity\CurrentDeals;
use App\Entity\Deal;
use App\Entity\TablePartRow;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 */
class BonusController extends AbstractController
{
    const ORDER_PERCENT_ADDED_TO_BONUS = 10;
    const MAX_ORDER_PERCENT_PAID_OFF_BY_BONUS = 30;
    const BIRTHDAY_BONUS = 600;
    const REFERRAL_BONUS = 300;

    /**
     * @Route("/partial-bonus-payent", name="partial-bonus-payment")
     */
    public function partialBonusPayment(): Response
    {
//        Если необходимо начислять бонусы в день рождения:
//        Создать таблицу с полями id клиента, дата рождения, в каком году последний раз получены бонусы за день рождения
//        В каждом методе вызывать метод, который будет вытягивать информацию по клиенту
//        Если она есть - сравнивать месяц и день рождения с текущим
//        При совпадении - начислять на счет клиента бонус и менять год последнего получения бонусов на текущий

//        Либо сделать вызов ручным, вместо того чтобы пихать его в каждый метод


//        ПОЛУЧИЛИ ДАННЫЕ ИЗ БИТРИКС24
        $orderId = '1';
        $clientId = '2';
        $orderSum = 2000;
        $bonusPaymentSum = 200;
        $bonusPaymentPercent = 10;
        $tablePart = [
            new TablePartRow('Product1', 500, 1),
            new TablePartRow('Product2', 500, 1),
            new TablePartRow('Product3', 1000, 2)
        ];

//        ПОГРУЗИЛИ ДАННЫЕ В СУЩНОСТЬ
        $deal = new Deal();
        $deal->setOrderId($orderId);
        $deal->setClientId($clientId);
        $deal->setOrderSum($orderSum);
        $deal->setBonusPaymentSum($bonusPaymentSum);
        $deal->setBonusPaymentPercent($bonusPaymentPercent);
        $deal->setTablePart($tablePart);

        $client = $this->findClient($deal->getClientId());

        if(empty($client)){
            $this->createNewClient($deal->getClientId());

            return $this->json([
                'message' => 'There has not been a client with this id yet.',
            ]);
        }

        if(empty($deal->getBonusPaymentSum()) && empty($deal->getBonusPaymentPercent())){

            return $this->json([
                'message' => 'The amount or percentage of bonuses to be debited is not specified.',
            ]);
        }

        $clientBonusAmount = $client->getBonusAmount();


        if(!empty($deal->getBonusPaymentSum())){
            $deal->setBonusPaymentPercent($deal->getOrderSum() / $deal->getBonusPaymentSum());
        }

        if(empty($deal->getBonusPaymentSum())){
            $deal->setBonusPaymentSum(($deal->getOrderSum() / 100) * $deal->getBonusPaymentPercent());
        }

        $bonusPaymentPercent = $deal->getBonusPaymentPercent();
            if($bonusPaymentPercent > $this::MAX_ORDER_PERCENT_PAID_OFF_BY_BONUS){

                return $this->json([
                    'message' => 'The possible percentage of payment for the order with bonuses has been exceeded.',
                ]);
            }
            if($clientBonusAmount < ($deal->getOrderSum() / 100) * $deal->getBonusPaymentPercent()){

                return $this->json([
                    'message' => 'There are not enough bonuses on the clients bonus account.',
                ]);
            }

        $deal->calculateDiscount($bonusPaymentPercent);


//        ПЕРЕЗАПИСАТЬ СДЕЛКУ
//        Если все четко
//        Отправить измененную сделку на битрикс

        $this->createCurrentDeal($deal);
        $this->bonusDebit($deal, $clientBonusAmount);

        return $this->json([
            'message' => 'Все четко.'
        ]);
    }

    /**
     * @Route("/deal-complited", name="deal-complited")
     */
    public function dealComplited(): Response
    {
        //        Получили id заказа, id клиента и сумму заказа
        $deal_id = 1;
        $client_id = 2;
        $bonusSum = 10;

        if(!empty($this->findCurrentDeal($deal_id))){
            $this->deleteCurrentDeal($deal_id);
        }else{
            if(empty($client = $this->findClient($client_id))){
                $this->createNewClient($client_id);
            }
            $this->addBonus($deal_id, $bonusSum);
        }

        return $this->json([
            'message' => 'Все четко.',
        ]);
    }

    /**
     * @Route("/deal-canceled", name="deal-canceled")
     */
    public function dealCanceled(): Response
    {
        //        Получили id заказа, id клиента и сумму заказа
        $deal_id = 1;
        $client_id = 2;
        $bonusSum = 10;

        if(!empty($this->findCurrentDeal($deal_id))){
            $this->addBonus($client_id, $bonusSum);
            $this->deleteCurrentDeal($deal_id);
        }

        return $this->json([
            'message' => 'Все четко.',
        ]);
    }

    /**
     * @param string $id
     * @return void
     */
    private function createNewClient(string $id): void
    {
        $client = new Clients();
        $client->setClientId($id);
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($client);
        $entityManager->flush();
    }

    /**
     * @param string $clientId
     * @return mixed|object|null
     */
    private function findClient(string $clientId)
    {
        $repository = $this->getDoctrine()->getRepository(Clients::class);

        return($repository->findOneBy(array('client_id' => $clientId)));
    }

    /**
     * @param $dealId
     * @return mixed|object|null
     */
    private function  findCurrentDeal($dealId)
    {
        $repository = $this->getDoctrine()->getRepository(CurrentDeals::class);

        return($repository->findOneBy(array('deal_id' => $dealId)));
    }

    /**
     * @param Deal $deal
     * @return void
     */
    private function createCurrentDeal(Deal $deal): void
    {
        $currentDeal = new CurrentDeals();
        $currentDeal->setClientId($deal->getClientId());
        $currentDeal->setDealId($deal->getOrderId());
        $currentDeal->setBonuses($deal->getBonusPaymentSum());
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($currentDeal);
        $entityManager->flush();
    }

    /**
     * @param $deal_id
     * @return void
     */
    private function deleteCurrentDeal($deal_id): void
    {
        $entityManager = $this->getDoctrine()->getManager();
        $conn = $entityManager->getConnection();
        $sql = "DELETE
        FROM current_deals
        WHERE deal_id = '$deal_id'
        ";
    }

    /**
     * @param Deal $deal
     * @param int $clientBonusAmount
     * @return void
     */
    private function bonusDebit(Deal $deal, int $clientBonusAmount): void
    {
        $newClientBonusAmount = $clientBonusAmount - $deal->getBonusPaymentSum();
        $clientId = $deal->getClientId();
        $entityManager = $this->getDoctrine()->getManager();
        $conn = $entityManager->getConnection();
        $sql = "UPDATE Clients
        SET bonus_amount = '$newClientBonusAmount'
        WHERE client_id = $clientId
        ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    }

    // МОЖНО ПОДВЯЗАТЬ ЧЕРЕЗ ХУКИ К ДНЮ РОЖДЕНИЯ И РЕФЕРАЛКЕ

    /**
     * @param string $clientId
     * @param int $addedBonusNumber
     * @return void
     */
    private function addBonus(string $clientId, int $addedBonusNumber): void
    {
        $client = $this->findClient($clientId);
        $bonusAmount = $client->getBonusAmount() + $addedBonusNumber;
        $entityManager = $this->getDoctrine()->getManager();
        $conn = $entityManager->getConnection();
        $sql = "UPDATE Clients
        SET bonus_amount = '$bonusAmount'
        WHERE client_id = $clientId
        ";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    }
}
