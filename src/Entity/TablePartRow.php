<?php

namespace App\Entity;

/**
 *
 */
class TablePartRow
{
    /**
     * @var
     */
    private $name;

    /**
     * @var
     */
    private $sum;

    /**
     * @var
     */
    private $number;

    /**
     * @var
     */
    private $discountSum;

    /**
     * @var
     */
    private $discountPercent;

    /**
     * @param $name
     * @param $sum
     * @param $number
     * @param $discountSum
     * @param $discountPercent
     * @param $discountType
     */
    public function __construct($name, $sum, $number)
    {
        $this->setName($name);
        $this->setSum($sum);
        $this->setNumber($number);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSum()
    {
        return $this->sum;
    }

    /**
     * @param mixed $sum
     */
    public function setSum($sum): void
    {
        $this->sum = $sum;
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number): void
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getDiscountSum()
    {
        return $this->discountSum;
    }

    /**
     * @param mixed $discountSum
     */
    public function setDiscountSum($discountSum): void
    {
        $this->discountSum = $discountSum;
    }

    /**
     * @return mixed
     */
    public function getDiscountPercent()
    {
        return $this->discountPercent;
    }

    /**
     * @param mixed $discountPercent
     */
    public function setDiscountPercent($discountPercent): void
    {
        $this->discountPercent = $discountPercent;
    }


}