<?php

namespace App\Entity;

use App\Repository\CurrentDealsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CurrentDealsRepository::class)
 */
class CurrentDeals
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $deal_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $client_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $bonuses;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDealId(): ?string
    {
        return $this->deal_id;
    }

    public function setDealId(string $deal_id): self
    {
        $this->deal_id = $deal_id;

        return $this;
    }

    public function getClientId(): ?string
    {
        return $this->client_id;
    }

    public function setClientId(string $client_id): self
    {
        $this->client_id = $client_id;

        return $this;
    }

    public function getBonuses(): ?int
    {
        return $this->bonuses;
    }

    public function setBonuses(int $bonuses): self
    {
        $this->bonuses = $bonuses;

        return $this;
    }
}
