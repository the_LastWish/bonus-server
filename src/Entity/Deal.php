<?php

namespace App\Entity;

/**
 *
 */
class Deal
{
    /**
     * @var
     */
    private $orderId;

    /**
     * @var
     */
    private $clientId;

    /**
     * @var
     */
    private $orderSum;

    /**
     * @var
     */
    private $bonusPaymentSum;

    /**
     * @var
     */
    private $bonusPaymentPercent;

    /**
     * @var
     */
    private $tablePart;

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId): void
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param mixed $clientId
     */
    public function setClientId($clientId): void
    {
        $this->clientId = $clientId;
    }

    /**
     * @return mixed
     */
    public function getOrderSum()
    {
        return $this->orderSum;
    }

    /**
     * @param mixed $orderSum
     */
    public function setOrderSum($orderSum): void
    {
        $this->orderSum = $orderSum;
    }

    /**
     * @return mixed
     */
    public function getBonusPaymentSum()
    {
        return $this->bonusPaymentSum;
    }

    /**
     * @param int|null $bonusPaymentSum
     * @return void
     */
    public function setBonusPaymentSum(?int $bonusPaymentSum): void
    {
        $this->bonusPaymentSum = $bonusPaymentSum;
    }

    /**
     * @return mixed
     */
    public function getBonusPaymentPercent()
    {
        return $this->bonusPaymentPercent;
    }

    /**
     * @param int|null $bonusPaymentPercent
     * @return void
     */
    public function setBonusPaymentPercent(?int $bonusPaymentPercent): void
    {
        $this->bonusPaymentPercent = $bonusPaymentPercent;
    }

    /**
     * @return mixed
     */
    public function getTablePart()
    {
        return $this->tablePart;
    }

    /**
     * @param mixed $tablePart
     */
    public function setTablePart($tablePart): void
    {
        $this->tablePart = $tablePart;
    }

    /**
     * @param int $discountPercent
     * @return void
     */
    public function calculateDiscount(int $discountPercent): void
    {
        foreach ($this->tablePart as $tablePart){
            $tablePartSum = $tablePart->getSum();
            $discountSum = ($tablePartSum / 100) * $discountPercent;
            $tablePart->setDiscountPercent($discountPercent);
            $tablePart->setDiscountSum($discountSum);
            $tablePart->setSum($tablePartSum - $discountSum);
        }
    }


}