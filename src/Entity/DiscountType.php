<?php

namespace App\Entity;
/** todo удалить если не понадобится */

/**
 *
 */
class DiscountType
{
    /**
     * @var
     */
    private $percent;

    /**
     * @var
     */
    private $absoluteNumber;

    public function __construct($percent, $absoluteNumber)
    {
        $this->setPercent($percent);
        $this->setAbsoluteNumber($absoluteNumber);
    }
    /**
     * @return mixed
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @param mixed $percent
     */
    public function setPercent($percent): void
    {
        $this->percent = $percent;
    }

    /**
     * @return mixed
     */
    public function getAbsoluteNumber()
    {
        return $this->absoluteNumber;
    }

    /**
     * @param mixed $absoluteNumber
     */
    public function setAbsoluteNumber($absoluteNumber): void
    {
        $this->absoluteNumber = $absoluteNumber;
    }


}